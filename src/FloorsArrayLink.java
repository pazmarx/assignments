public class FloorsArrayLink
{
    private FloorsArrayLink[] forarr;
    private FloorsArrayLink[] backarr;
    private double key;
    private int arrSize;

    public FloorsArrayLink(double key, int arrSize)
    {
        this.forarr = new FloorsArrayLink[arrSize];
        this.backarr = new FloorsArrayLink[arrSize];
        this.key = key;
        this.arrSize = arrSize;
    }

    public double getKey()
    {
        return this.key;
    }

    public FloorsArrayLink getNext(int i)
    {
        i -= 1;
        if (i >= this.arrSize)
            return null;
        return forarr[i];
    }

    public FloorsArrayLink getPrev(int i)
    {
        i -= 1;
        if (i >= this.arrSize)
            return null;
        return backarr[i];
    }

    public void setNext(int i, FloorsArrayLink next)
    {
        i -= 1;

        if (i <= this.arrSize)
            this.forarr[i] = next;
    }

    public void setPrev(int i, FloorsArrayLink prev)
    {
        i -= 1;

        if (i <= this.arrSize)
            this.backarr[i] = prev;
    }

    public int getArrSize()
    {
        return this.arrSize;
    }
}