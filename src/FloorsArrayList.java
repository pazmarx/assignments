public class FloorsArrayList implements DynamicSet {
    private int listSize;
    private FloorsArrayLink minimum;
    private FloorsArrayLink maximum;
    private int tallest;

    public FloorsArrayList(int N){
        listSize = 0;
        minimum = new FloorsArrayLink(Double.NEGATIVE_INFINITY, N);
        maximum = new FloorsArrayLink(Double.POSITIVE_INFINITY, N);
        tallest = 0;
        for (int i=1; i<=N; i++){
            minimum.setNext(i, maximum);
            maximum.setPrev(i, minimum);
        }
    }

    public int getSize(){
        return listSize;
    }

    public FloorsArrayLink lookup(double key) {
        FloorsArrayLink curr = minimum;
        for (int i = tallest; i>0; i--)
        {
            if (curr.getKey()<key)
                curr = curr.getNext(i);
            else
                if(curr.getKey()>key)
                    curr = curr.getPrev(i);
            if (curr.getKey() == key)
                return curr;
        }
        return null;
    }

    public void insert(double key, int arrSize) {
        FloorsArrayLink newKey = new FloorsArrayLink(key, arrSize);
        listSize++;
        FloorsArrayLink curr = minimum;
        int j=minimum.getArrSize();
        while(j>0){
            while(curr.getNext(j).getKey()<key){
                curr=curr.getNext(j);
                j=curr.getArrSize();
            }
            j--;
        }

        newKey.setPrev(1,curr);
        newKey.setNext(1,curr.getNext(1));
        FloorsArrayLink currPrev = newKey.getPrev(1);
        currPrev.setNext(1, newKey);
        FloorsArrayLink currNext = newKey.getNext(1);
        currNext.setPrev(1,newKey);
        for(int i=2; i<=arrSize; i++){
            while(currNext.getArrSize()<i)
                currNext = currNext.getNext(i-1);

            currNext.setPrev(i, newKey);
            newKey.setNext(i, currNext);

            while(currPrev.getArrSize()<i)
                currPrev = currPrev.getPrev(i-1);

            currPrev.setNext(i, newKey);
            newKey.setPrev(i, currPrev);
        }
        if (arrSize > tallest)
            tallest = arrSize;
    }

    public void remove(FloorsArrayLink toRemove) {
        for (int i=1; i<=toRemove.getArrSize();i++){
            FloorsArrayLink nextToFix = toRemove.getNext(i);
            FloorsArrayLink prevToFix = toRemove.getPrev(i);
            nextToFix.setPrev(i, prevToFix);
            prevToFix.setNext(i, nextToFix);
        }
        int j = 1;
        while (minimum.getNext(j) != maximum)
            j++;
        tallest = j;
        listSize--;
    }

    public double successor(FloorsArrayLink link) {
        return link.getNext(1).getKey();
    }

    public double predecessor(FloorsArrayLink link) {
        return link.getPrev(1).getKey();
    }

    public double minimum() {
        double retVal;
        if (listSize==0)
            retVal = maximum.getKey();
        else
            retVal = minimum.getNext(1).getKey();
        return retVal;
    }

    public double maximum() {
        double retVal;
        if (listSize==0)
            retVal = minimum.getKey();
        else
            retVal = maximum.getPrev(1).getKey();
        return retVal;
    }
}